#!/bin/bash

# by Eriberto
# Create the manpage using txt2man command.

T2M_DATE="16 Mai 2013"
T2M_NAME=mac-robber
T2M_VERSION=1.02
T2M_LEVEL=1
T2M_DESC="collects data about allocated files in mounted filesystems"

# Don't change the following line
txt2man -d "$T2M_DATE" -t $T2M_NAME -r $T2M_NAME-$T2M_VERSION -s $T2M_LEVEL -v "$T2M_DESC" $T2M_NAME.txt > $T2M_NAME.$T2M_LEVEL
